[TOC]

**解决方案介绍**
===============
该解决方案基于华为云函数工作流FunctionGraph的OBS触发器，来帮助用户实现对OBS源站上传的文件创建刷新缓存任务的自动化。适用于源站是OBS桶，需要自动创建刷新缓存任务降低人力成本的场景。

解决方案实践详情页面：https://www.huaweicloud.com/solution/implementations/cdn-automatic-refresh.html

**架构图**
---------------
![方案架构](./document/CDN-automated-refresh.PNG)

**架构描述**
---------------
该解决方案会部署如下资源：

1.在对象存储服务(OBS)上创建一个桶，作为OBS源站桶。

2.在统一身份认证服务(IAM)上创建一个委托，用于授权函数计算服务(FunctionGraph)访问用户在OBS源站桶和调用CDN创建刷新缓存任务API。

3.在函数工作流服务(FunctionGraph)上创建一个刷新缓存函数和OBS触发器，实现OBS源站上传文件创建刷新缓存任务的自动化。

**组织结构**
---------------
``` lua
huaweicloud-solution-cdn-automatic-refresh
├── cdn-automatic-refresh.tf.json -- 资源编排模板
├── functiongraph
    ├── CDN-automated-refresh.py  -- 函数文件
```
**开始使用**
---------------

1、登录华为云控制台，区域选择“华北-北京四”

图1 华为云控制台

![华为云控制台](./document/readme-image-001.PNG)

2、进入函数工作流FunctionGraph控制台，在函数列表中查看该方案创建的函数

图2 创建的函数

![创建的函数](./document/readme-image-002.PNG)

3、进入创建的函数中，查看该方案创建的触发器信息。

图3 CDN自动刷新缓存函数触发器

![CDN自动刷新缓存函数触发器](./document/readme-image-003.PNG)

4、进入统一身份认证服务控制台，查看已创建的委托信息

图4 委托信息

![委托信息](./document/readme-image-004.PNG)

5、在函数服务的监控中，可以查看触发器在文件上传到指定桶后自动创建缓存预热任务。

图5 请求日志

![请求日志](./document/readme-image-005.PNG)

6、在内容分发网络的预热刷新的历史记录也没中，可以看到刚创建的刷新缓存任务。

图6 刷新缓存任务

![刷新缓存任务](./document/readme-image-006.PNG)
